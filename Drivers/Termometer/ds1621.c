/*
******************************************************************
  @Filename		ds1621.c
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			25-09-2014
  @Description	This file contains services to control DS1621 chip. 
*******************************************************************
  */

#include "ds1621.h"
#include "ds1621_cfg.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_i2c.h"

//DS1621 commands codes
#define DS1621_COMMMAND_READTEMP (uint8_t)0xAA
#define DS1621_COMMMAND_ACCSESSTH (uint8_t)0xA1
#define DS1621_COMMMAND_ACCSESSTL (uint8_t)0xA2
#define DS1621_COMMMAND_ACCSESSCONF (uint8_t)0xAC
#define DS1621_COMMMAND_READCOUNTER (uint8_t)0xA8
#define DS1621_COMMMAND_READSLOPE (uint8_t)0xA9
#define DS1621_COMMMAND_STARTCONV (uint8_t)0xEE
#define DS1621_COMMMAND_STOPCONV (uint8_t)0x22


// @Description		DS1621 init function
// @Parameters		none
// @ReturnValue		none
void DS1621_Init()
{
  //Switch on i2c hardware module
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO | RCC_APB2Periph_GPIOB, ENABLE);
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
  
  GPIO_InitTypeDef gpio_init_struct;
  gpio_init_struct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
  gpio_init_struct.GPIO_Speed = GPIO_Speed_2MHz;
  gpio_init_struct.GPIO_Mode = GPIO_Mode_AF_OD;
  GPIO_Init(GPIOB, &gpio_init_struct);
  //Configure i2c module
  I2C_InitTypeDef i2c_init_struct;
  I2C_StructInit(&i2c_init_struct);
  i2c_init_struct.I2C_ClockSpeed = 100000;
  I2C_Init(DS1621_I2C_MODULE, &i2c_init_struct);
  I2C_Cmd(DS1621_I2C_MODULE, ENABLE);
  
  
}


// @Description		Command DS1621 to start convertion
// @Parameters		none
// @ReturnValue		Operation result. See ds1621_result_t type for details.
ds1621_result_t DS1621_ConvertionStart()
{
    //Send start convertion command
	I2C_GenerateSTART(DS1621_I2C_MODULE, ENABLE);
	while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
	I2C_Send7bitAddress(DS1621_I2C_MODULE, DS1621_ADDRESS, I2C_Direction_Transmitter);
	while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) == ERROR);
	I2C_SendData(DS1621_I2C_MODULE, DS1621_COMMMAND_STARTCONV);
	while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_BYTE_TRANSMITTED) == ERROR);
	I2C_GenerateSTOP(DS1621_I2C_MODULE, ENABLE);
	return(DS1621_RES_OK);
}


// @Description		Reads DS1621 configuration
// @Parameters		Reference to memory, where configuration will be copied.
// @ReturnValue		Operation result. See ds1621_result_t type for details.
ds1621_result_t DS1621_ConfigurationRead(uint8_t* config)
{
  I2C_GenerateSTART(DS1621_I2C_MODULE, ENABLE);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
  I2C_Send7bitAddress(DS1621_I2C_MODULE, DS1621_ADDRESS, I2C_Direction_Transmitter);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) == ERROR);
  I2C_SendData(DS1621_I2C_MODULE, DS1621_COMMMAND_ACCSESSCONF);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_BYTE_TRANSMITTED) == ERROR);
  
  I2C_GenerateSTART(DS1621_I2C_MODULE, ENABLE);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_MODE_SELECT) == ERROR);
  I2C_Send7bitAddress(DS1621_I2C_MODULE, DS1621_ADDRESS, I2C_Direction_Receiver);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) == ERROR);
  I2C_AcknowledgeConfig(DS1621_I2C_MODULE, DISABLE);
  I2C_GenerateSTOP(DS1621_I2C_MODULE, ENABLE);
  while( I2C_CheckEvent(DS1621_I2C_MODULE, I2C_EVENT_MASTER_BYTE_RECEIVED) == ERROR);
  *config = I2C_ReceiveData(DS1621_I2C_MODULE);
  //Read data here
   return(DS1621_RES_OK);
}


// @Description		Reads current temperature code
// @Parameters		Reference to memory, where temperature code will be put.
// @ReturnValue		Operation result. See ds1621_result_t type for details.
ds1621_result_t DS1621_TemperatureGet(uint16_t* temp_code)
{
  
  return(DS1621_RES_OK);
}