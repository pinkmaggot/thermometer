/*
******************************************************************
  @Filename		ds1621.h
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			25-09-2014
  @Description	Contains function prototypes for DS1621 chip library. 
*******************************************************************
  */

#ifndef DS1621_H
#define DS1621_H

#include <stdint.h>

typedef enum{
  DS1621_RES_OK, 
  DS1621_RES_BUSBUSY
  } ds1621_result_t;

  
//Provided Functions prototypes
void DS1621_Init();
ds1621_result_t DS1621_ConvertionStart();
ds1621_result_t DS1621_ConfigurationRead(uint8_t* config);


#endif