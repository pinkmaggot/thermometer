/*
******************************************************************
  @Filename		termometer.h
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			21-09-2014
  @Description	This file contents prototypes for termometer library
*******************************************************************
  */

#ifndef TERMOMETER_H
#define TERMOMETER_H

#include <stdint.h>

typedef uint8_t Temperature_t;

void TermometerInit();
Temperature_t TermometerGetTemp();

#endif

