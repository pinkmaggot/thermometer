/*
******************************************************************
  @Filename		termometer.c
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			25-09-2014
  @Description	This file contains all termometer functions 
*******************************************************************
  */

#include "termometer.h"
#include "ds1621.h"
  
// @Description		Termometer initialization function
// @Parameters		none
// @ReturnValue		none
void TermometerInit()
{
}


// @Description		Service to get current measured temperature
// @Parameters		none
// @ReturnValue		Measured temperature
Temperature_t TermometerGetTemp()
{
   
   return(0);
}