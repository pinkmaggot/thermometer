/*
******************************************************************
  @Filename		ds1621_cfg.h
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			25-09-2014
  @Description	This file contains configuration parameters for DS1621 driver.
*******************************************************************
  */
#ifndef DS1621_CFG
#define DS1621_CFG

#define DS1621_ADDRESS 0x90
#define DS1621_I2C_MODULE I2C1

#endif
