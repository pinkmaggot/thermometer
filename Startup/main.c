/*
******************************************************************
  @Filename		main.c
  @Author		Vladimir Golovnin
  @Version		1.0.0
  @Date			20-10-2014
  @Description	This file contains main system loop. 
*******************************************************************
  */

//Library headers include
#include "stm32f10x.h"
#include "Drivers/Termometer/ds1621.h"


uint8_t config=0;

//Main System loop 
void main()
{
  DS1621_Init();
  DS1621_ConvertionStart();
  DS1621_ConfigurationRead(&config);
  for(;;);
}