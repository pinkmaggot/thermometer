//systeminit.c
//��������� ������������� ����������������.

#include "stm32f10x.h"
char ClockOk=0;

void SystemInit (void)
{
  uint32_t StartUpCounter = 0, HSEStatus = 0;
  
  //����������� ����������� �����������
  SCB->AIRCR = (uint32_t)0x05FA0300;
  
  /* �������� HSE */    
  RCC->CR |= RCC_CR_HSEON;
 
  /* ���� ���� HSE ���������� � ������� �������� */
  while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT))
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;  
  } 
	//���� HSE ����������, �� �������� �������� PLL
	if(HSEStatus != 0)
	{
		HSEStatus=1;
		//����������� ��������� PLL � ������������.
		RCC->CFGR |= (uint32_t)(RCC_CFGR_PLLSRC_PREDIV1 | RCC_CFGR_PLLXTPRE_PREDIV1_Div2 | RCC_CFGR_PLLMULL6);
		//�������� PLL
		RCC->CR |= RCC_CR_PLLON;
		StartUpCounter=0;
		uint32_t PLLstatus=0;
		//���� ������ PLL � ������� ������� ��������
		while((PLLstatus == 0) && (StartUpCounter < HSE_STARTUP_TIMEOUT))
		{
			StartUpCounter++;
			PLLstatus = RCC->CR & RCC_CR_PLLRDY;
		}
		//���� PLL ���������� �������� ������������ �� ����.
		if(PLLstatus != 0)
		{
			PLLstatus=1;
			//����������� ������������ ��� ��� APB1, APB2
			RCC->CFGR |= RCC_CFGR_PPRE1_DIV1 | RCC_CFGR_PPRE2_DIV1;
			//������������� �� PLL
			RCC->CFGR |= RCC_CFGR_SW_PLL;
			StartUpCounter=0;
			//���� ������������
			while( ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL) && (StartUpCounter++ < HSE_STARTUP_TIMEOUT));
			ClockOk=1;
		}
		//���� PLL �� ����������, �������� ������������ �� HSE
		else
		{
			RCC->CFGR |= RCC_CFGR_SW_HSE;
			StartUpCounter=0;
			while( ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSE) && (StartUpCounter++ < HSE_STARTUP_TIMEOUT));
		}
	}
	
	//���� �� ���������� HSE, ��������� ������������ �� HSI.
	else
	{
		
		RCC->CFGR |= RCC_CFGR_HPRE_DIV1;
	}
}